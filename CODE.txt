#include "mbed.h"
#include "platform/mbed_thread.h"                                              

#define MPU6500_address 0xD0
#define GYRO_FULL_SCALE_250_DPS 0x00
#define GYRO_FULL_SCALE_500_DPS 0x08
#define GYRO_FULL_SCALE_1000_DPS 0x10
#define GYRO_FULL_SCALE_2000_DPS 0x18

#define ACC_FULL_SCALE_2_G 0x00
#define ACC_FULL_SCALE_4_G 0x08
#define ACC_FULL_SCALE_8_G 0x10
#define ACC_FULL_SCALE_16_G 0x18

#define SENSITIVITY_ACCEL 2.0/32768.0
#define SENSITIVITY_GYRO 250.0/32768.0
#define SENSITIVITY_TEMP 333.87
#define TEMP_OFFSET 21
#define SENSITIVITY_MAGN (10.0*4800.0)/32768.0

int16_t raw_accelx, raw_accely, raw_accelz;
int16_t raw_gyrox, raw_gyroy, raw_gyroz;
int16_t raw_temp;

float accelx, accely, accelz;
float gyrox, gyroy, gyroz;
float  temp;

char cm2[2];
char data[1];
char GirAcel[14];

float buffer[500][8];
int i;
Timer t;
float timer=0;

Serial pc(SERIAL_TX, SERIAL_RX);
I2C i2c(PB_9, PB_8); //SDA,SCL

int main()
{
    cm2[0] = 0x6B;
    cm2[1] = 0x00;
    i2c.write(MPU6500_address, cm2, 2);
    
    cm2[0] = 0x75;
    i2c.write(MPU6500_address, cm2, 1);
    i2c.read(MPU6500_address, data, 1);
    if(data[0] == 0x68){
        pc.printf("Error de conexion \n\r");
        pc.printf("Data: %#x \n\r",data[0]);
        pc.printf("\n\r");
        while(1);        
    }else{
        pc.printf("Conexion aceptada \n\r");
        pc.printf("Bienvenidos al Futuro \n\r");
        pc.printf("M");
        wait(0.3);
        pc.printf("O");
        wait(0.3);
        pc.printf("D");
        wait(0.3);
        pc.printf("E");
        wait(0.3);
        pc.printf("L");
        wait(0.3);
        pc.printf("O");
        wait(0.5);
        pc.printf(" ");
        wait(0.3);
        pc.printf("1");
        wait(0.3);
        pc.printf(".");
        wait(0.3);
        pc.printf("0 \n\r");
        wait(0.3);
        pc.printf("Para leer el sensor, presione la letra O");
        pc.printf("\n\r");
    }
    wait(0.1);
    
    cm2[0] = 0x1B;
    cm2[1] = 0x00;
    i2c.write(MPU6500_address, cm2, 2);
    
    cm2[0] = 0x1C;
    cm2[1] = 0x00;
    i2c.write(MPU6500_address, cm2, 2);
    wait(0.01);

    while (true) {
        if(pc.getc() == 'O'){
            for(i=0;i<=499; i++){
                cm2[0] = 0x3B;
                i2c.write(MPU6500_address, cm2, 1);
                i2c.read(MPU6500_address, GirAcel, 14);
                t.reset();
                t.start();
                raw_accelx = GirAcel[0]<<8 | GirAcel[1];
                raw_accely = GirAcel[2]<<8 | GirAcel[3];
                raw_accelz = GirAcel[4]<<8 | GirAcel[5];
                raw_temp = GirAcel[6]<<8 | GirAcel[7];
                raw_gyrox = GirAcel[8]<<8 | GirAcel[9];
                raw_gyroy = GirAcel[10]<<8 | GirAcel[11];
                raw_gyroz = GirAcel[12]<<8 | GirAcel[13];
                
                accelx = raw_accelx*SENSITIVITY_ACCEL;
                accely = raw_accely*SENSITIVITY_ACCEL;
                accelz = raw_accelz*SENSITIVITY_ACCEL;
                gyrox = raw_accelx*SENSITIVITY_GYRO;
                gyroy = raw_accely*SENSITIVITY_GYRO;
                gyroz = raw_accelz*SENSITIVITY_GYRO;
                temp = (raw_temp/SENSITIVITY_TEMP)+21;
                wait_ms(10);
                wait_ms(200);
                t.stop();
                timer = t.read();
                pc.printf("El tiempo es %f segundos \r", timer);
                pc.printf("%d %.2f %.2f %.2f %.2f %.2f %.2f %.2f \n\r",i+1,accelx, accely, accelz, gyrox, gyroy, gyroz, temp);
            }
        }
    }
}